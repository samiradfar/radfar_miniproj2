package samira.radfar.miniproj2_notes;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * Created by Delta-Group on 9/27/2016.
 */
public class ShrPrfs {
    SharedPreferences shr_prfs;
    SharedPreferences.Editor editor;
    private String Notes = "Notes";

    public ShrPrfs(Context context) {
        shr_prfs = context.getSharedPreferences("myapp" , Context.MODE_PRIVATE);
        editor = shr_prfs.edit();
    }

    public void setNotes (Notes[] notes ){
        Gson gson = new Gson();
        String output = gson.toJson(notes);
        editor.putString(Notes,output);
        editor.commit();
    }

    public Notes[] getnotes(){
        Gson gson = new Gson();
        String json = shr_prfs.getString(Notes,"");
        Notes[] notes = gson.fromJson(json,Notes[].class);
        return notes;
    }
}
