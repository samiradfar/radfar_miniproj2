package samira.radfar.miniproj2_notes;

/**
 * Created by Delta-Group on 9/27/2016.
 */
public class Notes {
    private String title;
    private String details;


    public Notes(String title, String details) {
        this.title = title;
        this.details = details;
    }

    public  String getTitle(){
        return title;
    }

    public String getDetails(){
        return details;
    }
}
