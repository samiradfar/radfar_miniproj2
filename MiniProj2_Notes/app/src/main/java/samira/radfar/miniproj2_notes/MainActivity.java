package samira.radfar.miniproj2_notes;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Dialog;
import android.app.FragmentManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.zip.Inflater;


public class MainActivity extends AppCompatActivity {

    myAdaptor adapter;
    ListView list1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnAdd = (Button) findViewById(R.id.btnAdd);
        list1 = (ListView) findViewById(R.id.list1);
        adapter = new myAdaptor();
        ShrPrfs shrPrfs = new ShrPrfs(MainActivity.this);
        Notes[] arrayNotes = shrPrfs.getnotes();
        if (arrayNotes != null){
            adapter.setNotesArray(arrayNotes);
            list1.setAdapter(adapter);
        }


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog dialog = new myDialog(){
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        super.onDismiss(dialog);
                        configView();
                    }
                };
                dialog.show(getFragmentManager() , "abc");
            }
        });
    }

    public void configView(){
        ShrPrfs shrPrfs = new ShrPrfs(MainActivity.this);
        Notes[] arrayNotes = shrPrfs.getnotes();
        if (arrayNotes != null){
            adapter.setNotesArray(arrayNotes);
            list1.setAdapter(adapter);
        }
        //adapter.notify();
    }

    public class myAdaptor extends BaseAdapter{

        Notes[] _notes;

        public void setNotesArray (Notes[] notes){
            _notes = notes;
        }

        @Override
        public int getCount() {
            return _notes.length;
        }

        @Override
        public Object getItem(int i) {
            return _notes[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.tablerow,parent,false);
            TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvTitle.setText(_notes[i].getTitle());
            return view;
        }
    }


    public static class myDialog extends android.app.DialogFragment {
        ShrPrfs shrprfs;
        EditText edTitle;
        EditText etDeyails;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            shrprfs = new ShrPrfs(getActivity());
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.adddialog , null);

            edTitle = (EditText) view.findViewById(R.id.etTitle);
            etDeyails = (EditText) view.findViewById(R.id.edDetails);
            Button btnSave = (Button) view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Title = edTitle.getText().toString();
                    String Details = etDeyails.getText().toString();

                    Notes[] mynotes = shrprfs.getnotes();
                    Notes newItem = new Notes(Title,Details);

                    int newlen=1;
                    if (mynotes != null)
                    {
                        newlen = mynotes.length+1;
                    }

                    Notes[] output = new Notes[newlen];
                    for(int i=0; i<newlen-1 ; i++){
                        output[i]=mynotes[i];
                    }
                    output[newlen-1] = newItem;
                    shrprfs.setNotes(output);

                    Toast.makeText(getActivity() , "Title "+Title+" , "+"Details "+Details , Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            });

            builder.setView(view);
            return builder.create();
        }
    }
}
